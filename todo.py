#!/usr/bin/env python3
"""
Todo

========================================
oooo    oooo               .
`888   .8P'              .o8
 888  d8'     .ooooo.  .o888oo  .oooo.
 88888[      d88' `88b   888   `P  )88b
 888`88b.    888   888   888    .oP"888
 888  `88b.  888   888   888 . d8(  888
o888o  o888o `Y8bod8P'   "888" `Y888""8o
               @nilsu.org
=== Copyright (C) 2018  Dakota Walsh ===
"""

# imports
import os       # paths
import sys      # sys.exit()
import argparse # arguments

# constants
VERSION           = "0.1.0"
HOME_DIR          = os.getenv("HOME", os.getenv("USERPROFILE")) # should be crossplatform
DATA_DIR          = os.path.join(HOME_DIR, ".local", "share")
CONFIG_DIR        = os.path.join(HOME_DIR, ".config")

# defaults
FOLDER_NAME           = 'todo.py'
DEFAULT_DATA          = 'default.csv'
DEFAULT_CONFIG        = 'config'
DEFAULT_ENCODING      = 'utf-8'
DEFAULT_DELIMITER     = ','
DEFAULT_SUB_DELIMITER = '|'

# ==============
# DATA SELECTION
#   FUNCTIONS
# ==============

def select_check_completed(entry):
	### return True if the entry is a member of selection all
	if (entry[2] == True):
		return(True)
	else:
		return(False)

def select_check_all(entry):
	### return True if the entry is a member of selection all
	if (entry[2] == False):
		return(True)
	else:
		return(False)

def select_data(verbose, data, selections):
	### return an updated version of data with only selected items ###
	sData = [] # the selected data to be returned
	if (verbose):
		print("Using selected data.")
	# TESTING: print selections
	print(selections)

	# loop through all entries in data
	for entry in data:
		if (selections[0] == True):
			# if selection "All" is needed
			if (select_check_all(entry)):
				sData.append(entry)
		if (selections[1] == True):
			# is selection "Completed" is needed
			if (select_check_completed(entry)):
				sData.append(entry)
	return(sData)

# ===============
# PARSE AND CLEAN 
#    FUNCTIONS
# ===============

def csv_split(text, delimiter):
	### stream csv text then return a python list ###
	i = 0
	tmp = ""
	partList = []
	# loop through the chars of the text
	for char in text:
		# increment i
		i = i + 1
		if (char != delimiter and char != "\n"):
			# if char is a normal character add it to tmp
			tmp = tmp + char
		if (char == delimiter):
			# if char is the delimiter
			# add tmp to partList and set tmp blank
			partList.append(tmp)
			tmp = ""
		if (i == len(text)):
			# if we're on the last char in text
			# add tmp to partList and set tmp blank
			partList.append(tmp)
			tmp = ""
	return(partList)

def clean_int(part):
	### convert a string to int safely ###
	if (part == ''):
		part = False
	else:
		part = int(part)
	return(part)

def clean_bool(part):
	### convert a string to bool safely ###
	if (part.lower() == "true"):
		part = True
	else:
		part = False
	return(part)

def clean_list(part, numbers):
	### convert a string to list safely ###
	### numbers is a bool; if the list items are integers it's true ### 

	# First we split our text into a python list
	if (not numbers):
		partList = csv_split(part, DEFAULT_SUB_DELIMITER)
		return(partList)
	else:
		cleanedPartList = []
		partList = csv_split(part, DEFAULT_SUB_DELIMITER)
		for part in partList:
			part = int(part)
			cleanedPartList.append(part)
		return(cleanedPartList)


def clean_data(verbose, data):
	### convert integers in the data to int ###
	### convert booleans in the data to bool ###
	### convert lists in the data to lists ###

	# check for verbosity
	if (verbose):
		print("Cleaning parsed data.")

	# loop through data by entry
	entryList = []
	for entry in data:
		# loop through each entry by part
		ii = 0
		partList = []
		for part in entry:
			# change the non-string parts
			if (ii == 0):
				part = clean_int(part)
				partList.append(part)
			elif (ii == 2):
				part = clean_bool(part)
				partList.append(part)
			elif (ii == 7):
				part = clean_list(part, True)
				partList.append(part)
			elif (ii == 8):
				part = clean_int(part)
				partList.append(part)
			elif (ii == 9):
				part = clean_int(part)
				partList.append(part)
			elif (ii == 11):
				part = clean_list(part, False)
				partList.append(part)
			else:
				partList.append(part)
			ii = ii + 1
		entryList.append(partList)

	return(entryList)

def parse_csv(verbose, dName):
	### read the dName file ###
	### return the contents as a list of lines ###
	### each line being a list of parts ###

	# check for verbosity
	if (verbose):
		print("Parsing csv database " + dName)

	# create variable to store todo list data
	entryList = []
	# load the csv file
	dNamePath = os.path.join(DATA_DIR, FOLDER_NAME, dName)
	with open(dNamePath) as dFile:
		# loop through each line as an entry
		for entry in dFile:
			# stream the csv line into a python list
			# also check for a sub delimiter and split into sub lists
			partList = csv_split(entry, DEFAULT_DELIMITER)

			# add that entry to our entryList
			entryList.append(partList)
	return(entryList)

# =========
# EDIT MODE
# FUNCTIONS
# =========

def mode_edit(verbose, dName, selections):
	### edit the users list ###
	pData = parse_csv(verbose, dName)
	cData = clean_data(verbose, pData)

# =========
# VIEW MODE
# FUNCTIONS
# =========

def mode_view(verbose, dName, selections):
	### view the users list ###
	pData = parse_csv(verbose, dName)
	cData = clean_data(verbose, pData)
	sData = select_data(verbose, cData, selections)
	# TESTING: Print sData
	for entry in sData:
		print(entry)

# ==============
# INITIALIZATION
#   FUNCTIONS
# ==============

def check_data(verbose, dName):
	### ensure the data folder and file exist ###
	if not os.path.isdir(DATA_DIR):
		# make the .local/share dir
		os.mkdir(DATA_DIR)
	if not os.path.isdir(os.path.join(DATA_DIR, FOLDER_NAME)):
		# make the .share/local/todo.py dir
		if (verbose):
			print("Data directory not found. Creating it.")
		os.mkdir(os.path.join(DATA_DIR, FOLDER_NAME))
	if not os.path.isfile(os.path.join(DATA_DIR, FOLDER_NAME, dName)):
		# make the default.csv file
		if (verbose):
			print("Data file not found. Creating it.")
		open(os.path.join(DATA_DIR, FOLDER_NAME, DEFAULT_DATA), 'a').close()
	else:
		if (verbose):
			print("Data file found.")

def check_config(verbose):
	### ensure the config folder and file exist ###
	if not os.path.isdir(CONFIG_DIR):
		# make the config dir
		os.mkdir(CONFIG_DIR)
	if not os.path.isdir(os.path.join(CONFIG_DIR, FOLDER_NAME)):
		# make the config/todo.py dir
		if (verbose):
			print("Config directory not found. Creating it.")
		os.mkdir(os.path.join(CONFIG_DIR, FOLDER_NAME))
	if not os.path.isfile(os.path.join(CONFIG_DIR, FOLDER_NAME, DEFAULT_CONFIG)):
		# make the config file
		if (verbose):
			print("Config file not found. Creating it.")
		open(os.path.join(CONFIG_DIR, FOLDER_NAME, DEFAULT_CONFIG), 'a').close()
	else:
		if (verbose):
			print("Config file found.")

def get_selection_args(arguments):
	### return a list on information about the selection args ###
	selections = []
	# 0 - BOL - All Uncompleted
	# 1 - BOL - All Completed
	# 2 - LST - IDs
	# 3 - LST - TAGs
	# 4 - INT - Priority (negative = min, possitive = max)
	# 5 - STR - Due Date
	# 6 - LST - Names

	# --all
	if (arguments.all):
		selections.append(True)
	else:
		selections.append(False)

	# --completed
	if (arguments.completed):
		selections.append(True)
	else:
		selections.append(False)

	# --id
	if (arguments.id):
		# store ids to selections list
		ids = arguments.id
		selections.append(ids)
	else:
		selections.append(False)

	# --tag
	if (arguments.tag):
		# store the tags to selection list
		tags = arguments.tag
		selections.append(tags)
	else:
		selections.append(False)

	# --priority
	if (arguments.priority):
		# store the priority value to selection list
		priority = arguments.priority
		selections.append(priority)
	else:
		selections.append(False)

	# --due-date
	if (arguments.due_date):
		# store the due date string to the selection list
		due_date = arguments.due_date
		selections.append(due_date)
	else:
		selections.append(False)

	# --name
	if (arguments.name):
		# store the names to the selection list
		names = arguments.name
		selections.append(names)
	else:
		selections.append(False)

	# --default
	if not any((arguments.all, arguments.completed, arguments.id)):
		selections[0] = True

	return(selections)

def get_args():
	### get the arguments with argparse ###
	description = "Todo list tool"
	arg = argparse.ArgumentParser(description=description)

	arg.add_argument("--version", action="store_true",
			help="Print todo.py version")

	arg.add_argument("--verbose", action="store_true",
			help="Verbose output")

	arg.add_argument("-l", "--list",
			help="Specify the list to load")

	arg.add_argument("-v", "--view", action="store_true",
			help="Use view mode")

	arg.add_argument("-e", "--edit", action="store_true",
			help="Use edit mode")

	arg.add_argument("-a", "--all", action="store_true",
			help="Select all incomplete tasks")

	arg.add_argument("-c", "--completed", action="store_true",
			help="Select all completed tasks")

	arg.add_argument("-i", "--id",
			help="Select tasks by id(s)")

	arg.add_argument("-t", "--tag",
			help="Select tasks by tag(s)")

	arg.add_argument("-p", "--priority",
			help="Select tasks by a min or max priority")

	arg.add_argument("-d", "--due-date",
			help="Select tasks by a min or max due date")

	arg.add_argument("-n", "--name",
			help="Select tasks by name(s)")

	return arg.parse_args()

def main():
	### keep track of and prioritize tasks ###
	# collect the parsed arguments from the command line
	arguments = get_args()

	# get a list of which selection args are being used
	selections = get_selection_args(arguments)

	# if version is requested print then exit
	if arguments.version:
		print("todo", VERSION)
		sys.exit()

	# store verbosity variable
	verbose = arguments.verbose

	# store the current database list to use
	if (arguments.list):
		dName = arguments.list
	else:
		dName = DEFAULT_DATA

	# determine the mode
	# mode = 0 - view
	# mode = 1 - edit
	# set deafult mode to view
	mode = 0
	if (arguments.view and arguments.edit):
		# Can't use both modes. Throw error.
		print("Error: You must select view mode (-v) OR edit mode (-e)")
		sys.exit(1)

	if (arguments.view):
		# use view mode
		mode = 0
		if (verbose):
			print("View mode selected")

	if (arguments.edit):
		# use edit mode
		mode = 1
		if (verbose):
			print("Edit mode selected")

	# check for the existence of needed files
	check_data(verbose, dName)
	check_config(verbose)

	# initialization complete
	if (verbose):
		print("Initialization complete.")
		if (mode == 0):
			print("Now entering view mode.")
		if (mode == 1):
			print("New entering edit mode.")
	if (mode == 0):
		mode_view(verbose, dName, selections)
	if (mode == 1):
		mode_edit(verbose, dName, selections)

if __name__ == '__main__':
	main()
