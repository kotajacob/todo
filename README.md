# `Todo.py`

Todo is a little command line program to keep track of and prioritize tasks.

## Use

**Todo is currently being developed and is not quite yet usable!**

My todo program is a bit more intelligent than your standard todo program or plain 
old notebook list. It not only keeps track of your full todo list (and all the 
items you've completed), but it also lists them back to you in the order by 
which you should complete them. Todo takes into account due dates, a priority 
value, the date you may start working on the entry, the time they were added 
to your list, and even an estimation of how long the item will take to complete.
All provide you with a good list of which items to work on first.

I created this program out of necessity as a busy college student with a job 
and probably far too many hobbies. I had been using a [bullet journal](http://bulletjournal.com/) 
like system on my computer for my todo list, but eventually realized there had 
to be a better way. Thus todo was born. Both the program, format, and all 
documentation are free to use, distribute, and modify under the 
terms of the MIT license.

Todo has two modes. View mode and edit mode. View mode is used simply for 
printing the todo list. Edit mode is an interactive mode which is used to make 
changes to, add, or remove entries from your todo list.

### View mode

View mode is the default mode when running todo, but it can be explicitly 
called with the `-v` option.

A view mode command is made up of three parts.
	
 - A selection of the entries you would like to view.  
 - A method of how to sort the selected entries.  
 - An output formatting option.  

When view mode is run with no options the options below are used.  
If any one of the three parts are missing it is filled in with the respective 
part from the default command.

`todo -v -a -s 'date|priority|added|id' --pretty`  

Some options may take a list as an input. A list is seperated by the `|` symbol.
If you need to include a literal `|` symbol you may include it by simply 
placing two next to each other as shown `||`

Below are tables of the options for each of the three parts.  

By default all of the uncompleted options are selected, if you'd rather say 
only select items tagged as homework you could use `-t 'homework'`

| Selection Options         | Description                                        |
|---------------------------|----------------------------------------------------|
| `-a` and `--all`          | Select all list entries (except completed entries) |
| `-i` and `--id`           | A list of entry IDs to select                      |
| `-t` and `--tag`          | A list of entry tags to select                     |
| `-p` and `--priority`     | A min or max priority for selecting entries        |
| `-d` and `--due-date`     | A due date range for selecting entries             |
| `-n` and `--name`         | A list of entry names to select                    |
| `-c` and `--completed`    | Select all completed entries                       |

If you're unsure of the sorting options it's suggested to leave them out and 
use the defaults.

| Sorting Options           | Description                                                                  |
|---------------------------|------------------------------------------------------------------------------|
| `-s` and `--sort`         | Use a specific sorting preset                                                |
| `-m` and `--manual-sort`  | Manually select the sorting attributes from order of most to least important |

| Manual Sorting Attributes |                                                        |
|---------------------------|--------------------------------------------------------|
| `date`                    | The adjusted due date                                  |
| `priority`                | The adjusted priority                                  |
| `added`                   | The date added to the todo list                        |
| `id`                      | The ID of the entry (lower values having priority)     |

The default `--pretty` option looks great in a terminal, but if you're putting 
the output into a text file for printing `--minimal` may be nicer. Finally if 
you've made a simple gui or tui to read and display the csv lines you can use 
`--raw` for that.

| Display Options | Description                                                                           |
|-----------------|---------------------------------------------------------------------------------------|
| --pretty        | The standard table display of the entries                                             |
| --minimal       | A very minimal list of the entries (good for printing)                                |
| --raw           | A raw listing of the csv lines for the entries (could be passed into another program) |

### Edit mode

You can enter edit mode simply by running todo `-e`

Edit mode is in development currently and cannot be used quite yet.
Although any standard text editor and most spreadsheet programs may be 
used to edit your todo list, since it is just a simple csv file.

### Additional Options

In addition to the view mode and edit more options there are a few other 
miscellaneous command line options.

| Miscellaneous Options | Description                        |
|-----------------------|------------------------------------|
| `--version`           | Print todo version then exit       |
| `--verbose`           | Print additional debug information |
| `-l` and `--list`     | Specify the name of the todo list  |


## Data Storage

todo.py stores its configuration in `~/.config/todo.py/`
it stores its data in `~/.local/share/todo.py/`

Data is stored in seperate files for each todo list.
The files are in the very common csv file format, which is a plain text 
spreadsheet format that uses the `,` character to seperate items.

Each line is a seperate todo list entry.
A todo list entry contains 12 different data points.

Below is a table of the data points in order.

| #  | Name            | Description                                                                                    |
|----|-----------------|------------------------------------------------------------------------------------------------|
| 1  | ID              | A unique number given to the entry upon its creation                                           |
| 2  | Name            | A short name for your todo list item                                                           |
| 3  | Completed       | True or False if the item has been completed                                                   |
| 4  | Date Created    | yyyy-mm-dd formatted date of when the item was added to your list                              |
| 5  | Date Changed    | yyyy-mm-dd formatted date of the last time the item was modified by todo                       |
| 6  | Date Due        | yyyy-mm-dd formatted date of which the entry should be completed                               |
| 7  | Date Due Start  | yyyy-mm-dd formatted date of when you may begin work on the item                               |
| 8  | Requires        | A list of the IDs of any other todo items which must be completed before working on this entry |
| 9  | Priority        | A number value representing the importance of the entry by itself                              |
| 10 | Completion Time | An estimation of the time in minutes to complete the entry                                     |
| 11 | Description     | A description of the entry in detail                                                           |
| 12 | Tags            | A list of tags to associate with the entry                                                     |

As with the command line options list data points are seperated with 
a `|` symbol. Entering an actual `|` symbol can be done by placing two of them
next to each other like so `||`

## License

Todo.py is licensed under the very liberal MIT license. Please read LICENSE in
this repo for more information.

Copyright (c) 2018 Dakota Walsh
